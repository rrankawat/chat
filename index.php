<?php

session_start();

if(isset($_GET['user'])) {
	$_SESSION['user'] = (int)$_GET['user'];
}
else {
	$_SESSION['user'] = 0;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Ajax Chat</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="chat">
		<div class="messages"></div>
		<textarea class="entry" placeholder="Type a message..."></textarea>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="js/chat.js"></script>
</body>
</html>